# Maternity Allowance Service Calculator API Tests

This project contains a set of API tests for the Maternity Allowance Service Calculator. The tests are written using Rest Assured and are executed against the Calculator API.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Docker version 19.03.12 or later
- Git
- OpenJDK 8
- Maven

### Running the tests

The tests are run in a Docker container. The Docker image is built using the Dockerfile.dev file and tagged as `ma-service-calculator`.

The tests are cloned from a GitLab repository and run using Maven. The specific branch used for the tests is `b.palanisamy`.

The tests are run using the `mvn verify -Dtest=CucumberRunner` command.

### Test Reports

After the tests are run, a report is generated and saved in the `ma-pathfinder-automation-api/target/cucumber/cluecumber/index.html` path. This report can be viewed in a web browser. (not yet)

## Built With

- Docker
- Rest Assured
- Maven
- GitLab CI/CD

## Contributing

Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the LICENSE.md file for details.
